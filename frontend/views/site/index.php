

            <!-- home-section 
                    ================================================== -->
            <section id="home-section" class="slider1">

                <!--
                #################################
                        - THEMEPUNCH BANNER -
                #################################
                -->
                <div class="tp-banner-container">
                    <div class="tp-banner" >
                        <ul>	<!-- SLIDE  -->
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
                                <!-- MAIN IMAGE -->
                                <img src="<?= FRONTENDURL;?>/upload/slide/slide_1.jpg"  alt="slidebg1" data-lazyload="<?= FRONTENDURL;?>/upload/slide/slide_1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="200"
                                     data-y="190" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="left-top corner-border"></span>
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
                                     data-x="200"
                                     data-y="330" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="left-bottom corner-border"></span>
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="900"
                                     data-y="190" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="right-top corner-border"></span>
                                </div>

                                <!-- LAYER NR. 4 -->
                                <div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
                                     data-x="900"
                                     data-y="330" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="right-bottom corner-border"></span>
                                </div>

                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption finewide_medium_white lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="320"
                                     data-y="270" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1200"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">Moving can't be easier
                                </div>

                            </li>

                            <!-- SLIDE  -->
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-saveperformance="on"  data-title="Ken Burns Slide">
                                <!-- MAIN IMAGE -->
                                <img src="<?= FRONTENDURL;?>/images/dummy.png"  alt="2" data-lazyload="<?= FRONTENDURL;?>/upload/slide/slide_2.jpg" data-bgposition="right top" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center bottom">
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="200"
                                     data-y="190" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="left-top corner-border"></span>
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
                                     data-x="200"
                                     data-y="330" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="left-bottom corner-border"></span>
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="900"
                                     data-y="190" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="right-top corner-border"></span>
                                </div>

                                <!-- LAYER NR. 4 -->
                                <div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
                                     data-x="900"
                                     data-y="330" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="right-bottom corner-border"></span>
                                </div>

                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption finewide_medium_white lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="340"
                                     data-y="270" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1200"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">Believe your dream
                                </div>
                            </li>
                            <!-- SLIDE  -->
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on"  data-title="Parallax 3D">
                                <!-- MAIN IMAGE -->
                                <img src="<?= FRONTENDURL;?>/upload/slide/slide_3.jpg"  alt="3dbg" data-lazyload="<?= FRONTENDURL;?>/upload/slide/slide_3.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="200"
                                     data-y="190" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="left-top corner-border"></span>
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
                                     data-x="200"
                                     data-y="330" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="left-bottom corner-border"></span>
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="900"
                                     data-y="190" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="right-top corner-border"></span>
                                </div>

                                <!-- LAYER NR. 4 -->
                                <div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
                                     data-x="900"
                                     data-y="330" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1000"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><span class="right-bottom corner-border"></span>
                                </div>

                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption finewide_medium_white lft tp-resizeme rs-parallaxlevel-0"
                                     data-x="345"
                                     data-y="270" 
                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="1200"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">We have tradition
                                </div>
                            </li>
                        </ul>
                        <div class="tp-bannertimer"></div>
                    </div>
                </div>
            </section>
            <!-- End home section -->

            <!-- banner-section 
                    ================================================== -->
            <section class="banner-section">
                <div class="container">
                    <h2>All You Need for transport Creative &amp; Professional<a href="<?= DOMAINURL?>/site/place-order" class="button-one">Get Free Quote</a></h2>
                </div>
            </section>
            <!-- End banner section -->

            <!-- services-offer 
                    ================================================== -->
            <section class="services-offer-section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <a href="<?= FRONTENDURL;?>/site/howworks"<button class="btn btn-lg btn-primary">How it works</button></a>
                            
                        </div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="offer-post">
                                <a href="" class="no-link"><img src="<?= FRONTENDURL;?>/upload/others/con22.jpg" alt=""></a>
                                <h2><a href="" class="no-link">PREPARING</a></h2>
                                <p>We provide packaging facilities for your shipment at your doorstep.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="offer-post">
                                <a href="" class="no-link"><img src="<?= FRONTENDURL;?>/upload/others/con2.jpg" alt=""></a>
                                <h2><a href="" class="no-link">COLLECTING</a></h2>
                                <p>We collect your shipment at your location and at your convinient time.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="offer-post">
                                <a href="" class="no-link"><img src="<?= FRONTENDURL;?>/upload/others/con3.jpg" alt=""></a>
                                <h2><a href="" class="no-link">Reassembling</a></h2>
                                <p>Do you have anything that breaks! dont worry our expert team will make sure it is repacked.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End services-offer section -->

            <!-- tabs-section 
                    ================================================== -->
            <section class="tabs-section">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-7">
                            <div class="about-us-box">
                                <h1>about us and our priorities</h1>
                                <p>Pioneering the customized truck body building business in 1980s, now we emerged as conglomerate of three major business verticals involving manufacturing, sales and service, and hydel power production.
</p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="about-us-post">
                                            <a href="#"><i class="fa fa-dropbox"></i></a>
                                            <h2>Packaging</h2>
                                            <span>We package &amp; store</span>
                                        </div>
                                        <div class="about-us-post">
                                            <a href="#"><i class="fa fa-globe"></i></a>
                                            <h2>Worldwide</h2>
                                            <span>Everywhere</span>
                                        </div>
                                        <div class="about-us-post">
                                            <a href="#"><i class="fa fa-plane"></i></a>
                                            <h2>Plane</h2>
                                            <span>Faster with plane</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="about-us-post">
                                            <a href="#"><i class="fa fa-truck"></i></a>
                                            <h2>Transporting</h2>
                                            <span>We fast transport</span>
                                        </div>
                                        <div class="about-us-post">
                                            <a href="#"><i class="fa fa-hand-lizard-o"></i></a>
                                            <h2>Hand to Hand</h2>
                                            <span>Secure Transport</span>
                                        </div>
                                        <div class="about-us-post">
                                            <a href="#"><i class="fa fa-user-secret"></i></a>
                                            <h2>Secure</h2>
                                            <span>We guarantee</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-5">

                            <div class="about-box">
                                <img src="<?= FRONTENDURL;?>/upload/others/iso1.jpg" alt="">
                                <h2>Who we are</h2>
                                <p>MyLorry are one of the choicest companies that offers integrated solutions for on-road and off-road commercial vehicle requirements through custom body manufacturing, sales, and service of trucks and trailers for construction, mining, and for logistics operators.</p>
                            </div>

                        </div>

                    </div>
                </div>
            </section>
            <!-- End tabs section -->

            
          
            
        


