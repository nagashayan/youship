<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About MyLorry';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="container about-us">
        <div class="row">
            <div class="col-xs-12">
    <h2 class="text-center">How it works</h2>
    <h3>As a customer,</h3>
    <ul>
        <li>	Enter details of your consignment in detail.</li>
        <li>Provide us with your pick – up point and delivery location.</li>
        <li>You can either provide your own price or ask our operators for a quote. </li>
        <li>We respect your privacy , An operator can only get in touch with you only if you accept the quote.</li>
    </ul>
    <h3>As a operator,</h3>
    <ul><li>	Our Operators will be notified with the quotes and requirements sent by you. </li>
    <li>	Operator can accept the most suitable quote. Your booking is confirmed. </li>
    <li>	Vehicles arrives at your specified location to load the materials. </li>
    </ul>
    <h3>Rate your experience,</h3>
    <ul><li>	You can rate the operator once the transaction is completed.</li>

Exciting Feature – For the first Time in History of Consignment Online Booking, 
We have provided our users with a two way bargain system. This means, you may not be very much satisfied with the operators quote.
<li>	As a customer, you can counter(bargain) the operators quote. Operator can accept or provide a new Quote.<li>
<li>	If you are happy with the quote accept it and If you are still unhappy you can reject the quote, but after this rejection the operator cannot reach you anymore.
</li>
    </ul>
        </div>
        </div>
            </div>
        </div>
