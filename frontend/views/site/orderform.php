<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $ordermodel common\models\Orders */
/* @var $orderinfomodel common\models\OrderInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>Place your order</h3>
                <?php if(!$profile){ ?>
                <div class="alert alert-danger">Please create your <a href="<?= DOMAINURL."/profile"?>">profile</a> to continue</div>
                <?php } else {?>
                <div class="order-info-form">
                    <?php
                    $form = ActiveForm::begin([
                              
                    ]);
                    ?>
                    <div class="row">
                        <div class="col-xs-12 col-lg-9"> 
                            <?= $form->field($ordermodel, 'title')->textInput(['maxlength' => true])->label("Type of Goods") ?>

                            <?= $form->field($ordermodel, 'description')->textArea(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                       
                        <div class="col-xs-4 col-lg-3 gps-img-parent">
                            <?= $form->field($ordermodel, 'pickuplocation')->textInput(['maxlength' => true]);?>
                            <img class="gps-symbol" onclick="getLocation('#orders-pickuplocation')" src="<?=DOMAINURL;?>/images/gps.png"/>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?=
                            $form->field($ordermodel, 'pickup_city')->textInput(['maxlength' => true]);
                            ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?=
                            $form->field($ordermodel, 'pickuplocationtype')->textInput(['maxlength' => true])->dropDownList(
                                    ['residential' => 'Residential', 'office' => 'Office or Commercial']
                            );
                            ?>
                        </div>
                        
                    </div>
                    <!-- <div class="form-group field-orders-pickupdate2 ">
                         <label class="control-label" for="orders-pickupdate2">Pickup Date</label>
                         <? DatePicker::widget([
                     'name' => 'pickupdate2', 
                     'value' => date('d-M-Y', strtotime('+4 days')),
                     'options' => ['placeholder' => 'Select issue date ...'],
                     'pluginOptions' => [
                         'format' => 'dd-M-yyyy',
                         'todayHighlight' => true
                     ]
                     ]);
                     ?>
                         
                     </div>
                     
                     
                     
                 
                     <? $form->field($ordermodel, 'pickupcond')->textInput(['maxlength' => true]) ->dropDownList(
                             ['before'=>'Before','after'=>'After','between'=>'Between']
                            
                         );
                  ?>
                    -->
                    <div class="row">
                        <div class="col-xs-4 col-lg-3 gps-img-parent">
                            <?= $form->field($ordermodel, 'deliverylocation')->textInput(['maxlength' => true]) ?>
                            <img class="gps-symbol" onclick="getLocation('#orders-deliverylocation')" src="<?=DOMAINURL;?>/images/gps.png"/>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?=
                            $form->field($ordermodel, 'delivery_city')->textInput(['maxlength' => true]);
                            ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?=
                            $form->field($ordermodel, 'deliverylocationtype')->textInput(['maxlength' => true])->dropDownList(
                                    ['residential' => 'Residential', 'office' => 'Office or Commercial']
                            );
                            ?>
                        </div>
                        
                    </div>
                    <!--  <div class="form-group field-orders-deliverydate2  ">
                          <label class="control-label" for="orders-deliverydate2">Delivery Date</label>
                          <? DatePicker::widget([
                      'name' => 'deliverydate2', 
                      'value' => date('d-M-Y', strtotime('+2 days')),
                      'options' => ['placeholder' => 'Select issue date ...'],
                      'pluginOptions' => [
                          'format' => 'dd-M-yyyy',
                          'todayHighlight' => true
                      ]
                      ]);
                      ?>
                          
                      </div>
                  
                      <? $form->field($ordermodel, 'deliverycond')->textInput(['maxlength' => true])->dropDownList(
                              ['before'=>'Before','after'=>'After','between'=>'Between']
                             
                          ); ?>-->
                  
                      
                    <div class="row">
                        <div class="col-xs-4 col-lg-3">
                            <div class="form-group field-orders-pickupdate1 required ">
                                
                                <?=
                                 $form->field($ordermodel, 'pickupdate1')->widget(DatePicker::classname(), [
                                    'name' => 'pickupdate1',
                                    'id' => 'datepicker1',
                                    'value' => date('Y-m-d'),
                                    
                                    'options' => ['placeholder' => 'Select pickup date ...'],
                                    'pluginOptions' => [
                                        'startDate' => date('Y-m-d'),
                                        'format' => 'yyyy-m-dd',
                                        'todayHighlight' => true
                                    ]
                                ]);
                                ?>

                            </div>
                        </div>
                        
                            <div class="col-xs-4 col-lg-3">
                            <div class="form-group field-orders-deliverydate1 required ">
                                
                                <?=
                                 $form->field($ordermodel, 'deliverydate1')->widget(DatePicker::classname(), [
                                   // 'model' => $ordermodel, 
                                    //'attribute' => 'deliverydate1',
                                    'id' => 'datepicker2',
                                    'name' => 'deliverydate1',
                                    'value' => date('Y-m-d', strtotime('+2 days')),
                                    'options' => ['placeholder' => 'Select delivery date ...'],
                                    'pluginOptions' => [
                                        'startDate' => date('Y-m-d'),
                                        'format' => 'yyyy-m-dd',
                                        'todayHighlight' => true
                                    ]
                                ]);
                                ?>

                        </div>
                        </div>
                        
                    </div>
                    
                      <div class="row section-break">
                          <div class="col-xs-4 col-lg-6">
                              <h3>Consignment Details</h3>
                          </div>
                      </div>
                    <!--  order info -->
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="alert alert-danger">
                                Hazardous material cannot be carried in any form
                            </div>
                        </div>
                    </div>
                    
                   <!--  <div class="row">
                        <div class="col-xs-4 col-lg-3">
                            <? $form->field($orderinfomodel, 'weight')->textInput()->label("Weight (In tons)") ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?
                            $form->field($orderinfomodel, 'breakable')->textInput()->dropDownList(
                                    ['0' => 'No', '1' => 'Yes']
                            );
                            ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?
                            $form->field($orderinfomodel, 'wooden')->textInput()->dropDownList(
                                    ['0' => 'No', '1' => 'Yes']
                            );
                            ?>
                        </div>
                    </div> -->
                    
                     <div class="row">
                         <div class="col-xs-12 col-lg-12">
                           <?= $form->field($orderinfomodel, 'typeofload')->inline()->radioList([
                                '1' => 'Part Load',
                                '2' => 'Full Load',
                            ],['onchange'=>"typeloadchange()"])->label("Type of Load");
                            ?>
                             
                         </div>
                     </div>
                    <div class="row">
                        <div class="col-xs-3" id="first_list_div">
                            
                            <?=
                            $form->field($orderinfomodel, 'typeofpartload')->textInput()->dropDownList(
                                    [1 => 'Solid', 2 => 'Liquid', 3=>'Gas'],['id'=>'first_list','prompt' => ' -- select appropriate option --','onchange'=>'subtypeload()']
                            )->label(false);
                            ?>
                        </div>
                         <div class="col-xs-3" id="second_list_div">
                            
                            <?=
                            $form->field($orderinfomodel, 'typeoffullload')->textInput()->dropDownList(
                                    [1 => 'Container', 2 => 'Open Body'],['id'=>'second_list','prompt' => ' -- select appropriate option --','onchange'=>'subtypeload()']
                            )->label(false);
                            ?>
                        </div>
                    </div>
                    <div class="row" id="solidoptions">
                        <div class="col-xs-12">
                            <div class="row">
                        <div class="col-xs-4 col-lg-3">

                            <?= $form->field($orderinfomodel, 'width')->textInput()->label("Width (In inches)") ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?= $form->field($orderinfomodel, 'height')->textInput()->label("Height (In inches)") ?>
                                                    </div>
                        <div class="col-xs-3">
                            <?= $form->field($orderinfomodel, 'length')->textInput()->label("Length (In inches)") ?>
                        </div>
                            </div>
                           <div class="row">
                        <div class="col-xs-4 col-lg-3">

                            <?= $form->field($orderinfomodel, 'weight')->textInput()->label("Weight (In inches)") ?>
                        </div>
                        
                    </div> 
                        </div>
                    </div>
                   
                    <div class="row" id="liquidoptions">
                        <div class="col-xs-4 col-lg-3">

                            <?= $form->field($orderinfomodel, 'typeofliquid')->textInput()->label("Type of Liquid") ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?= $form->field($orderinfomodel, 'liquidlicense')->dropDownList(
                                    ['yes' => 'Yes', 'no' => 'No']
                            )->label("Any License Required") ?>
                                                    </div>
                                                    <div class="col-xs-3">
                            
                        </div>
                    </div>
                    <div class="row" id="gasoptions">
                        <div class="col-xs-4 col-lg-3">

                            <?= $form->field($orderinfomodel, 'typeofgas')->textInput()->label("Type of Gas") ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?= $form->field($orderinfomodel, 'gascontainertype')->dropDownList(
                                    ['cylinder' => 'Cylinder', 'tank' => 'Tank']
                            )->label("Container Type") ?>
                        </div>
                              
                    </div>
                    <div class="row" id="containeroptions">
                        <div class="col-xs-4 col-lg-3" >
                            <?= $form->field($orderinfomodel, 'containertype')->dropDownList(
                                    ['8ft 6" X 6ft 6"' => '8ft 6" X 6ft 6"', '14ft X 7ft' => '14ft X 7ft','19ft X 8ft'=>'19ft X 8ft',
                                        '24ftX 8ft'=>'24ftX 8ft','32ft X 8 ft'=>'32ft X 8 ft','custom'=>'Custom'],['onchange'=>'containertypechange()','id'=>'containersizeoption']
                            )->label("Container Type") ?>
                        </div>
                        <div class="col-xs-4 col-lg-3" id="customsize">

                            <?= $form->field($orderinfomodel, 'customsize')->textInput(['placeholder'=>'Enter width * height in feet'])->label("Enter Size") ?>
                        </div>
                        
                              
                    </div>
                    <div class="row" id="openbodyoptions">
                        <div class="col-xs-4 col-lg-3">

                            <?= $form->field($orderinfomodel, 'openbodysize')->textInput(['type' => 'number','step'=>"any"])->label("Enter Weight in Tons(Between 1.5 ton to 40 tons)") ?>
                        </div>
                       
                              
                    </div>
                   <div class="row">
                        <!-- <div class="col-xs-4 col-lg-3">
                            <?
                            $form->field($orderinfomodel, 'packed')->textInput()->dropDownList(
                                    ['0' => 'No', '1' => 'Yes']
                            );
                            ?>
                        </div>
                        <div class="col-xs-4 col-lg-3">
                            <?
                            $form->field($orderinfomodel, 'packagetype')->textInput(['maxlength' => true])->dropDownList(
                                    ['woodenitem' => 'Wooden Item', 'glass' => 'Glass']
                            );
                            ?>

                        </div>
                        -->
                        <div class="col-xs-4 col-lg-3">
                            <?= $form->field($ordermodel, 'offerprice')->textInput() ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton($ordermodel->isNewRecord ? 'Place Order' : 'Update Order', ['class' => $ordermodel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

            <?php ActiveForm::end(); ?>
                </div>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var locid = "";
   window.onload = function() {
   $("#pickupcurrentloc").change(function(){
     console.log("changed");
     if($('#pickupcurrentloc').is(":checked")){
         console.log("checked");
     }
     else{
         console.log("unchecked");
     }
   });
   

<?php if(!$orderinfomodel->isNewRecord){ ?>
        if($("input[name='OrderInfo[typeofload]']:checked").val() == "1"){
        $("#second_list").attr('disabled','disabled');
        $("#first_list").removeAttr('disabled');
        $("#second_list_div").hide();
        $("#first_list_div").show();
        $("#containeroptions").hide();
        $("#openbodyoptions").hide();
        subtypeload();
    }else{
        $("#second_list").removeAttr('disabled','disabled');
        $("#first_list").attr('disabled','disabled');
        $("#second_list_div").show();
        $("#first_list_div").hide();
        $("#solidoptions").hide();
        $("#liquidoptions").hide();
        $("#gasoptions").hide();
        subtypeload();
    }
<?php }?>

}; 
function getLocation(id) {
    locid = id;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getReverseGeocodingData);
    } else {
        $(locid).val( "Geolocation is not supported by this browser.");
    }
}
function showPosition(address) {
    
    $(locid).val(address);
    
}
function getReverseGeocodingData(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    
    var latlng = new google.maps.LatLng(lat, lng);
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status !== google.maps.GeocoderStatus.OK) {
            console.log(status);
        }
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
            console.log(results);
            var address = (results[0].formatted_address);
            console.log(address);
            showPosition(address);
        }
    });
}

  
    </script>