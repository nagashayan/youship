<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About MyLorry';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="container about-us">
        <div class="row">
            <div class="col-xs-12">
    <h1><?= Html::encode($this->title) ?></h1>
    <h3>Honesty is the best policy - a company built on trust and customer care</h3>
    <p style="padding-top: 10px;">
       Today’s most daunting and harried experience of moving things around. Let it be logistics solutions for a company or a business establishment or just relocating the house. 
The main problem factors are lack of timely availability of quality service, benchmark fares, resulting due to the unorganized nature of the transport business.

    </p>
        <!-- <p>
        Even if it loses a sale ! This is one of the key factors that has helped us grow exponentially over the years and setting us apart from our competitors.
        Our honesty, great customer care and attention to postage and packing has built an outstanding reputation which our business is built on. 
        Quality and price is key - no expensive old hat or cheap tat
        We filter our vendors very affectively and make sure our prices are competitively priced. Every vendor we list on our system has personally been checked, as we will not list any vendor that hasn't met his approval standards.
        </p>
        <p>
        We are upfront with our pricing - all prices include VAT, unlike many of our competitors who sting you with VAT charges at the checkout.
        MyLorry covers consolidated cargo service, direct delivery utilizing chartered trucks and freight container transportation. Star Transporter can propose the optimal distribution system for each customer through a tactical combination of our national main line transit network and our independent collection and delivery services linking local areas.
    </p>
-->

    <h2>Quality and price is key - no expensive old hat or cheap tat</h2>
    
    <p>
        To eliminate these factors,<strong> MyLorry </strong>is making the shifting and handling process seamless accessible and instant no matter where you are located. But lets not forget, we need your help to keep up your reach.
    </p>
    <ul>
    <li>
        	You should make sure that you know what you're shipping. 
    </li>
    <li>
        When you posting your items on <strong> MyLorry</strong> , do your best to find out the size, weight, and any special requirements that your shipment might need.
        
        </li>
        <li>
            If you're thinking about listing your household move on<strong> MyLorry</strong>,
        
            Make sure you are not underestimating the amount of stuff you are planning to load.<br>
        Making a detailed inventory list is very important and can help you if anything goes wrong during the move.
        We at MyLorry make sure our costumers can track what they have shipped.
        </li>
     </ul>
    <!-- <p>
    We filter our vendors very affectively and make sure our prices are competitively priced. Every vendor we list on our system has personally been checked, as we will not list any vendor that hasn't met his approval standards. We are upfront with our pricing - all prices include VAT, unlike many of our competitors who sting you with VAT charges at the checkout.
    MyLorry covers consolidated cargo service, direct delivery utilizing chartered trucks and freight container transportation. Star Transporter can propose the optimal distribution system for each customer through a tactical combination of our national main line transit network and our independent collection and delivery services linking local areas.
    </p> -->
    <p><strong>MyLorry</strong> A simple and user friendly approach.</p>
   
        </div>
        </div>
            </div>
        </div>
