<?php

/* @var $this yii\web\View */

$this->title = 'Order placed';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully placed your order.</p>

        <p><a class="btn btn-lg btn-success" href="<?= DOMAINURL?>/site/view-order">View your Quotes</a></p>
    </div>

</div>
