/*
 * Custom javascript file contains all necessary client side code
 */
var DOMAINURL = "http://localhost/youship/frontend/web";
$(".new-offer-price").keypress(function(e){
    console.log("key press"+$(this).attr("data-id")+e.keyCode);
    if(e.keyCode == 13){
        console.log("enter pressed");
        if($(this).val() == ""){
            alert("Please enter value");
        }
        else{
     //make ajax request with id and offer price
    $.post(DOMAINURL+"/site/update-customer-quote",
    {
        id: $(this).attr("data-id"),
        offerprice: $(this).val()
    },
    function(data, status){
        console.log("Data: " + data + "\nStatus: " + status);
        if(data == "enter again"){
            alert("Your quote should be less than operator quote");
            
        }
        window.location.href = DOMAINURL+"/site/view-order";
    });
}
}
});

$(".refresh-btn").click(function(){
   window.location.reload(); 
});
var submitquote = function(id){
        console.log("triggering");
        var order_id = "#"+id+"_order";
        console.log(id+order_id+$(order_id).val());
        if($(order_id).val() == ""){
            alert("Please enter value");
        }
        else{
          //make ajax request with id and offer price
            $.post(DOMAINURL+"/site/update-customer-quote",
            {
                id: id,
                offerprice: $(order_id).val()
            },
            function(data, status){
                console.log("Data: " + data + "\nStatus: " + status);
                if(data == "enter again"){
                    alert("Your quote should be less than operator quote");

                }
                window.location.href = DOMAINURL+"/site/view-order";
            });
        }
}


var typeloadchange = function(){
    console.log("changed");
    if($("input[name='OrderInfo[typeofload]']:checked").val() == "1"){
        $("#second_list").attr('disabled','disabled');
        $("#first_list").removeAttr('disabled');
        $("#second_list_div").hide();
        $("#first_list_div").show();
        $("#containeroptions").hide();
        $("#openbodyoptions").hide();
        subtypeload();
    }else{
        $("#second_list").removeAttr('disabled','disabled');
        $("#first_list").attr('disabled','disabled');
        $("#second_list_div").show();
        $("#first_list_div").hide();
        $("#solidoptions").hide();
        $("#liquidoptions").hide();
        $("#gasoptions").hide();
        subtypeload();
    }
}

var subtypeload = function(){
   console.log("changed"); 
   if($("input[name='OrderInfo[typeofload]']:checked").val() == "1"){
        //consider options from first list
        if($("#first_list").val() == 1){
            $("#solidoptions").show();
            $("#liquidoptions").hide();
            $("#gasoptions").hide();
        }
        else if($("#first_list").val() == 2){
            $("#solidoptions").hide();
            $("#liquidoptions").show();
            $("#gasoptions").hide();
        }
        else if($("#first_list").val() != ""){
            $("#solidoptions").hide();
            $("#liquidoptions").hide();
            $("#gasoptions").show();
        }
        else{
            $("#solidoptions").hide();
            $("#liquidoptions").hide();
            $("#gasoptions").hide();
        }
    }else{
        console.log("second options");
        //consider options from second list
        if($("#second_list").val() == 1){
            $("#containeroptions").show();
            $("#openbodyoptions").hide();
            
        }
        else if($("#second_list").val() == 2){
            $("#containeroptions").hide();
            $("#openbodyoptions").show();
            
        }
         else{
             $("#containeroptions").hide();
             $("#openbodyoptions").hide();
            
        }
    }
   
}

var containertypechange = function(){
    console.log("contianer size changed");
    if($("#containersizeoption").val() == "custom"){console.log("showing");
        $("#customsize").show();
    }
    else{console.log("hiding");
       $("#customsize").hide(); 
    }
}