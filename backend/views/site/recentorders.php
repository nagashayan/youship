<?php

/* @var $this yii\web\View */

$this->title = 'Lorry App';
?>

<style>
    
th,td{
        text-align: center;
    }
</style>
<div class="site-index">
<div class="container">
    <h2>Transporter Quotes</h2>
    
        <div class="row">
            <div class="col-xs-12">
                
           
   <table class="table table-hover">
    <thead>
      <tr>
        <th>From</th>
        <th>To</th>
        <th>Quoted Price</th>
      </tr>
    </thead>
    <tbody>
    
    <?php foreach($orderfeed as $order){ ?>
        
      <tr>
        <td><?= $order->pickup_city?></td>
        <td><?= $order->delivery_city?></td>
        <td class="text-center"><?php $operatorquote = $order->getLastOperatorQuote($order->id);
        echo ($operatorquote > 0) ?  $operatorquote : "-" ;?></td>
        
      </tr>
        
    <?php  } ?>
</tbody>
</table>
                 </div>
        </div>
    </div>
</div>
