<?php

/* @var $this yii\web\View */

$this->title = 'Lorry App';
?>

<style>
    
th,td{
        text-align: center;
    }
</style>
<div class="site-index">
<div class="container">
    <h2>Consignee Quotes</h2>
    
        <div class="row">
            <div class="col-xs-12">
                
           
   <table class="table table-hover">
    <thead>
      <tr>
        <th>From</th>
        <th>To</th>
        <th>Offer Price</th>
        <th>Status</th>
        <th>View details</th>
      </tr>
    </thead>
    <tbody>
    
    <?php foreach($orderfeed as $order){ ?>
        
      <tr>
        <td><?= $order->pickup_city?></td>
        <td><?= $order->delivery_city?></td>
        <td><?= ($order->offerprice > 0) ? $order->offerprice : "-" ?></td>
        <td><?= ($order->status == 1) ? 'Active' : (($order->status == 2) ? 'Order Completed' : 'Disabled'); ?></td>
        <td><a href="<?= BACKENDURL?>/site/view-complete-order?id=<?= $order->id;?>">View Details</a></td>
        
      </tr>
        
    <?php  } ?>
</tbody>
</table>
                 </div>
        </div>
    </div>
</div>
