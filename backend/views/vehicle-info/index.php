<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehicle Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-info-index">
 <div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Add Vehicle', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'vehicle_type',
            'vehicle_num',
            'otherinfo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
