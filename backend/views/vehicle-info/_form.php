<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehicle-info-form">
     <div class="container">
          <div class="row">
            <div class="col-lg-4">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vehicle_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vehicle_num')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'otherinfo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
            </div>
          </div>
     </div>
</div>
