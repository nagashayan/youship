<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\QuoteLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quote-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'order_id')->textInput() ?>

    <?= $form->field($model, 'offer_price')->textInput() ?>

    <?= $form->field($model, 'quote_from')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operator_id')->textInput() ?>

    <?= $form->field($model, 'quoted_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
