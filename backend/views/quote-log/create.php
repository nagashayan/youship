<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\QuoteLog */

$this->title = 'Create Quote Log';
$this->params['breadcrumbs'][] = ['label' => 'Quote Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quote-log-create">
<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
