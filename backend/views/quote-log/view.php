<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\QuoteLog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Quote Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quote-log-view">
<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'offer_price',
            'quote_from',
            'operator_id',
            'quoted_date',
        ],
    ]) ?>
</div>
</div>
