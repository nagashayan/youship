<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            //'auth_key',
            //'password_hash',
           // 'password_reset_token',
            // 'email:email',
            [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function ($model) {                      
                    if($model->status == 10)
                        return 'Enabled';
                    else
                        return 'Disabled';
            },
        ],
            // 'created_at',
            // 'updated_at',
                        [
              'attribute' => 'operator',
              'format' => 'raw',
              'value' => function ($model) {                      
                      if($model->operator == 1)
                          return 'Yes';
                      else
                          return 'No';
              },
          ],
                                [
           'attribute' => 'superuser',
           'format' => 'raw',
           'value' => function ($model) {                      
                   if($model->superuser == 1)
                       return 'Yes';
                   else
                       return 'No';
           },
       ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
