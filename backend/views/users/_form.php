<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <div class="row">
        <div class="col-xs-4">
    <?php $form = ActiveForm::begin(); ?>
    

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'password_hash')->textInput() ?>
    <?= $form->field($model, 'superuser')->dropDownList([0=>'No',1=>'Yes']) ?>
    <?= $form->field($model, 'operator')->dropDownList([0=>'No',1=>'Yes']) ?>
    <?= $form->field($model, 'status')->dropDownList([10=>'Enable',0=>'Disable']) ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
            </div>
    </div>
</div>
