<?php

namespace backend\controllers;

use Yii;
use common\models\Vehicleinfo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VehicleInfoController implements the CRUD actions for VehicleInfo model.
 */
class VehicleInfoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VehicleInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
         if(((new \common\models\User())->isSuperUser())){
            $vehicles = \common\models\Vehicleinfo::find();
        }
        else{
            $vehicles = Vehicleinfo::find()->where("userid = ".Yii::$app->user->id);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $vehicles,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VehicleInfo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if($model->userid == Yii::$app->user->id){
            return $this->render('view', [
                'model' => $model,
            ]);
        }
        else {
            return $this->render('error', [
                        'errormsg' => ACCESSDENIED
            ]);
        }
    }

    /**
     * Creates a new VehicleInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VehicleInfo();
        $model->userid = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VehicleInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->userid == Yii::$app->user->id){
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        }
        else {
            return $this->render('error', [
                        'errormsg' => ACCESSDENIED
            ]);
        }
    }

    /**
     * Deletes an existing VehicleInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($model->userid == Yii::$app->user->id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
        }
        else {
            return $this->render('error', [
                        'errormsg' => ACCESSDENIED
            ]);
        }
    }

    /**
     * Finds the VehicleInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VehicleInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VehicleInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
