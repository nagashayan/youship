<?php
return [
    'adminEmail' => 'admin@mylorry.com',
    'supportEmail' => 'support@mylorry.com',
    'user.passwordResetTokenExpire' => 3600,
];
