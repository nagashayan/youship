<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vehicleinfo".
 *
 * @property integer $id
 * @property string $vehicle_type
 * @property string $vehicle_num
 * @property string $otherinfo
 */
class Vehicleinfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicleinfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vehicle_num'], 'required'],
            [['vehicle_type'], 'string', 'max' => 100],
            [['vehicle_num'], 'string', 'max' => 50],
            [['otherinfo'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vehicle_type' => 'Vehicle Type',
            'vehicle_num' => 'Vehicle Num',
            'otherinfo' => 'Otherinfo',
        ];
    }
}
