<?php

namespace common\models;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property integer $userid
 * @property double $offerprice
 * @property string $pickuplocation
 * @property string $pickuplocationtype
 * @property string $pickupdate1
 * @property string $pickupdate2
 * @property string $pickupcond
 * @property string $deliverylocation
 * @property string $deliverylocationtype
 * @property string $deliverydate1
 * @property string $deliverydate2
 * @property string $deliverycond
 * @property string $createdon
 * @property string $updatedon
 * @property string $pickup_city
 * @property string $delivery_city
 *
 * @property OrderInfo[] $orderInfos
 * @property User $user
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status', 'userid', 'pickuplocation', 'pickuplocationtype', 'deliverylocation', 'deliverylocationtype', 
                'pickup_city','delivery_city','pickupdate1','deliverydate1'], 'required'],
            [['status', 'userid'], 'integer'],
            [['offerprice'], 'number'],
            [['pickupdate1', 'pickupdate2', 'deliverydate1', 'deliverydate2', 'createdon', 'updatedon'], 'safe'],
            [['title'], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 1000],
            [['pickuplocation', 'deliverylocation'], 'string', 'max' => 200],
            [['pickuplocationtype', 'pickupcond', 'deliverylocationtype', 'deliverycond'], 'string', 'max' => 100],
            ['deliverydate1', 'validateDate'],
        ];
    }
     public function validateDate($attribute, $params)
    {
        
         $date1 = new \DateTime($this->pickupdate1);
         $date2 = new \DateTime($this->deliverydate1);
         $diff = date_diff($date1, $date2);
         
         if($diff->invert == 1){ 
             $this->addError($attribute, 'Delivery date should be greater than or equal to pickup date');
        }

    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Type of Goods',
            'description' => 'Description of Goods',
            'status' => 'Status',
            'userid' => 'Userid',
            'offerprice' => 'Offerprice',
            'pickuplocation' => 'Pickup Address',
            'pickuplocationtype' => 'Pickup Location Type',
            'pickupdate1' => 'Pickup Date',
            'pickupdate2' => 'Pickupdate2',
            'pickupcond' => 'Pickupcond',
            'deliverylocation' => 'Delivery Address',
            'deliverylocationtype' => 'Delivery Location Type',
            'deliverydate1' => 'Delivery Date',
            'deliverydate2' => 'Deliverydate2',
            'deliverycond' => 'Deliverycond',
            'createdon' => 'Createdon',
            'updatedon' => 'Updatedon',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderInfos()
    {
        return $this->hasMany(OrderInfo::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
    
    public function behaviors(){
      return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdon',
                'updatedAtAttribute' => 'updatedon',
                'value' => new Expression('NOW()'),
            ],
        ];
      }
      
      /**
       * returns last quote by customer
       */
      public function getLastCustomerQuote($id){
          //get last quote for custom from quote log
          $quotelog = Quotelog::find()->where("order_id = $id and quote_from = 'customer'")->orderBy("quoted_date desc")->one();
          
          if(isset($quotelog->id)){
             return $quotelog->offer_price; 
          }
          else{
              //if quote log is empty
              return false;
          }
      }
      
      /**
       * returns last quote by operator
       */
      public function getLastOperatorQuote($id){
          //get last quote for custom from quote log
          $quotelog = Quotelog::find()->where("order_id = $id and quote_from = 'operator'")->orderBy("offer_price asc")->one();
          if(isset($quotelog->id)){
             return $quotelog->offer_price; 
          }
          else{
              //if quote log is empty
              return false;
          }
      }
      
      /**
       * get customer quote status
       * return false if last quote is customer or bidding is closed
       */
      public function getCustomerQuoteStatus($order){
          $id = $order->id;
          $quote = Quotelog::find()->where("order_id = $id")->orderBy("id desc")->one();
          if(isset($quote->id)){
             // if($quote->quote_from == "operator" && Quotelog::find()->where("order_id = $id")->orderBy("id desc")->count() < 4){
              if($quote->quote_from == "operator"){
                  return true;
              }
              
          }
          
          return false;
          
          
      }
      
      /**
       * get accepted quote details
       */
      public function acceptedQuote($id){
          $order = \common\models\Orders::find()->where("id = $id")->one();
        //get all quote info from quote log table
        
       
            //get quote details
            $quotelog = \common\models\Quotelog::find()->where("order_id = $id and operator_id = $order->accepted_operator")->orderBy("offer_price")->one();
           
            echo $quotelog->offer_price;
            

        
      }
      
      /**
       * get accepted quote details
       */
      public function acceptedOperator($id){
         
            //get operator profile
            $profile = \frontend\models\Profile::find()->where("user_id = $id")->one();
           
            return $profile;
            

        
      }
       /**
     * Sends an email when operator quotes or accepts order
     *
     * @return boolean whether the email was send
     */
    public function sendEmail($update = null)
    {
        /* @var $user User */
        $user = User::findOne([
            
            'id' => $this->userid,
        ]);
        $lastquote = $this->getLastOperatorQuote($this->id);
        if ($user) {
            
            if($update == null){
                return \Yii::$app->mailer->compose(['html' => 'orderupdate-html', 'text' => 'orderupdate-text'], ['user' => $user,'lastquote' => $lastquote,'order'=>$this])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
                    ->setTo($user->email)
                    ->setSubject('New quote for you order on ' . \Yii::$app->name)
                    ->send();
            }
            elseif($update == 'accepted'){
                return \Yii::$app->mailer->compose(['html' => 'orderaccept-html', 'text' => 'orderaccept-text'], ['user' => $user,'lastquote' => $lastquote,'order'=>$this])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
                    ->setTo($user->email)
                    ->setSubject('Congratulations! your order has been accepted ' . \Yii::$app->name)
                    ->send();
            }
            
        }

        return false;
    }
      
      
}
