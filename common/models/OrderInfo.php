<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_info".
 *
 * @property integer $id
 * @property integer $order_id
 * @property double $width
 * @property double $height
 * @property double $length
 * @property double $weight
 * @property integer $breakable
 * @property integer $wooden
 * @property integer $packed
 * @property string $packagetype
 * @property integer $openbodysize
 *
 * @property Orders $order
 */
class OrderInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id','typeofload'], 'required'],
            [['order_id'], 'integer'],
            [['width', 'height', 'length', 'weight'], 'number'],
            [['packagetype'], 'string', 'max' => 100],
            [['typeofload','typeofpartload','typeoffullload','typeofliquid','liquidlicense','typeofgas','gascontainertype'
                ,'containertype','customsize','openbodysize'], 'safe'],
            ['openbodysize','validateSize'],
        ];
    }
    
     public function validateSize($attribute, $params)
    {
        
         $size = $this->openbodysize;
//         $openbody = new \DateTime($this->openbody);
//         $fullload = new $this->fullload;
        if($size < 1.5 || $size > 40)
            $this->addError($attribute, 'Size should be greater than 1.5 tons and lesser than 40 tons');
         
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'width' => 'Width',
            'height' => 'Height',
            'length' => 'Length',
            'weight' => 'Weight',
            'breakable' => 'Breakable',
            'wooden' => 'Wooden',
            'packed' => 'Packed',
            'packagetype' => 'Package Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }
    
    
}
