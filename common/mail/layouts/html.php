<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div id=":31c" class="ii gt m153d2c900dd0ca34 adP adO"><div id=":345" class="a3s" style="overflow: hidden;"><div style="width:100%!important;background:#f2f2f2;margin:0;padding:0" bgcolor="#f2f2f2"><div class="adM">



</div><div><div class="adM">
    
    </div><table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" style="width:100%!important;line-height:100%!important;border-collapse:collapse;margin:0;padding:0">
        <tbody>
        <tr>
            <td style="border-collapse:collapse">
                <table width="542" bgcolor="white" cellpadding="0" cellspacing="0" border="0" align="center" style="display:block;border-collapse:collapse">
                    <tbody style="display:table;width:100%">
                    <tr>
                        <td style="border-collapse:collapse">
                            
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse">
                                <tbody>
                                <tr>
                                    <td valign="middle" width="100%" align="center" style="border-collapse:collapse;padding:20px 0">
                                        <div>
<a href="<?= FRONTENDURL?>" target="_blank">
                                                <img src="<?= FRONTENDURL?>/images/mylorrylogo.jpg" alt="mylorry" width="124" height="47" border="0" style="display:block;outline:none;text-decoration:none;border:none" class="CToWUd">
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    
</div>
<div>
    
    <div style="padding-left: 28px;padding-top: 15px;"><?= $content;?></div>
    
</div>
<div>
    
    <table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" style="width:100%!important;line-height:100%!important;border-collapse:collapse;margin:0;padding:0">
        <tbody>
        <tr>
            <td width="100%" style="border-collapse:collapse">
                <table width="502" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse">
                    <tbody>
                    
                    <tr>
                        <td width="100%" height="40" style="border-collapse:collapse"></td>
                    </tr>
                    
                   
                    
                    <tr>
                        <td width="100%" height="5" style="border-collapse:collapse"></td>
                    </tr>
                    
                                        
                    
                    <tr>
                        <td width="100%" height="5" style="border-collapse:collapse"></td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="middle" style="font-family:Helvetica,arial,sans-serif;font-size:10px;color:#666666;border-collapse:collapse">
                            Mylorry © 2016
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="100%" height="40" style="border-collapse:collapse"></td>
                    </tr>
                    
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    
</div>

    
  <?php $this->endBody() ?>   
</body>
</html>
<?php $this->endPage() ?>
