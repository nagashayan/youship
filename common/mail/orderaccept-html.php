<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = DOMAINURL.'/site/view-order';
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->username) ?>,</p>

    <p>Hurray! your order has been accepted for more info please login and visit your <?= Html::a(Html::encode('View orders page'), $resetLink) ?></p>

</div>
