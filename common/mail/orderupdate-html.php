<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = DOMAINURL.'/site/view-order';
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->username) ?>,</p>

    <p>Hurray! you have a new quote price of <?= $lastquote;?> for your order <?= $order->pickup_city?> To <?= $order->delivery_city;?>, please login and visit your <?= Html::a(Html::encode('View orders page'), $resetLink) ?></p>

</div>
